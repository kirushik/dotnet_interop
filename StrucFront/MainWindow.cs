﻿using System;
using Gtk;

using System.Runtime.InteropServices;

public struct WeirdUUID
{
	public int a;
	public int b;
	[MarshalAs (UnmanagedType.ByValArray, SizeConst = 16)]
	public byte[] uuid;

	public override string ToString ()
	{
		return string.Format ("[({0}, {1}), {2}]", this.a, this.b, BitConverter.ToString (this.uuid));
	}
}

public class RustLib
{
	[DllImport ("../../../strucsorter/target/debug/libstrucsorter.so", EntryPoint = "get_random")]
	public static extern bool getRandom (ref WeirdUUID uuid);

	[DllImport ("../../../strucsorter/target/debug/libstrucsorter.so", EntryPoint = "get_n_random")]
	public static extern bool getNRandom (int n, [Out] WeirdUUID[] uuid);
}

public partial class MainWindow: Gtk.Window
{

	public MainWindow () : base (Gtk.WindowType.Toplevel)
	{
		Build ();
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}

	protected void callPressed (object sender, EventArgs e)
	{
		var uuids = new WeirdUUID[10];
		for (int i = 0; i < 10; i++) {
			uuids [i].a = i;
			uuids [i].b = i * i;
			uuids [i].uuid = new byte[16];
		}

		RustLib.getNRandom (10, uuids);
		Console.WriteLine (string.Join (",\n", uuids));
	}
}
