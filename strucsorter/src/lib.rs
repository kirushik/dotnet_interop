#![crate_type = "dylib"]

extern crate rand;
use rand::{thread_rng, Rng};

#[repr(C)]
#[derive(Copy, Clone)]
pub struct RatedWeightedUUID {
    base: i32,
    weight: i32,
    uuid: [u8; 16]
}

#[no_mangle]
pub extern "C" fn get_random(x: &mut RatedWeightedUUID) -> bool {
    let mut rng = thread_rng();
    x.base = rng.gen();
    x.weight = rng.gen();
    rng.fill_bytes(&mut x.uuid);
    true
}

#[no_mangle]
pub extern "C" fn get_n_random(n: usize, x: &mut [RatedWeightedUUID]) -> bool {
    for i in 0..n {
        get_random(&mut x[i]);
    }
    true
}

#[test]
fn it_works() {
    let mut uuids = [RatedWeightedUUID { base: 0, weight: 0, uuid: [0u8;16] }; 10];
    get_n_random(10, &mut uuids);
}
